# falcon512

This repository contains a C library implementing Falcon, a post-quantum signature algorithm.

Falcon is based on NTRU lattices, used with a hash-and-sign structure
and a Fourier-based sampling method that allows efficient signature
generation and verification, while producing and using relatively
compact signatures and public keys. The official Falcon Web site is:

https://falcon-sign.info/

This implementation is written in C and is configurable at compile-time
through macros which are documented in config.h; each macro is a boolean
option and can be enabled or disabled in config.h and/or as a
command-line parameter to the compiler. Several implementation strategies
are available; however, in all cases, the same API is implemented.

This repository is a snapshot of the current state of falcon implementation that could
be found at:

https://falcon-sign.info/Falcon-impl-20211101.zip

The Falcon specification could be find here:

https://falcon-sign.info/falcon.pdf

## Authors and acknowledgment

Falcon is a cryptographic signature algorithm submitted to NIST Post-Quantum Cryptography Project
on November 30th, 2017. It has been designed by: Pierre-Alain Fouque, Jeffrey Hoffstein, Paul Kirchner,
Vadim Lyubashevsky, Thomas Pornin, Thomas Prest, Thomas Ricosset, Gregor Seiler, William Whyte, Zhenfei Zhang.

The code was written by Thomas Pornin (thomas.pornin@nccgroup.com), to
whom questions may be addressed.

## License

The implementation is provided under the [MIT licence](LICENSE.txt).


## Project status

This is not meant to be an repository used for actual development. It is instead a convenience place where to refer
to the original implementation exposed via git. The code will be updated to keep this repo in sync with the code
published at https://falcon-sign.info
